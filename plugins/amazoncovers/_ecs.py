# Copyright (C) 2006 Adam Olsen
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 1, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

import urllib
import sys
import hmac
import hashlib
import base64
import datetime
import re
from xl.nls import gettext as _
import logging

logger = logging.getLogger(__name__)

class AmazonSearchError(Exception): pass

def generate_timestamp():
    ret = datetime.datetime.utcnow()
    return ret.isoformat() + 'Z'

# make a valid RESTful AWS query, that is signed, from a dictionary
# http://docs.amazonwebservices.com/AWSECommerceService/latest/DG/index.html?RequestAuthenticationArticle.html
# code by Robert Wallis: SmilingRob@gmail.com, your hourly software contractor
def get_aws_query_string(aws_access_key_id, secret, query_dictionary):
	query_dictionary["AWSAccessKeyId"] = aws_access_key_id
	query_dictionary["Timestamp"] = generate_timestamp()
	query_pairs = map(
		lambda (k,v):(k+"="+urllib.quote(v)),
		query_dictionary.items()
	)
	 # The Amazon specs require a sorted list of arguments
	query_pairs.sort()
	query_string = "&".join(query_pairs)
	hm = hmac.new(
		secret,
		"GET\nwebservices.amazon.com\n/onca/xml\n"+query_string,
		hashlib.sha256
	)
	signature = urllib.quote(base64.b64encode(hm.digest()))
	query_string = "https://webservices.amazon.com/onca/xml?%s&Signature=%s" % (query_string, signature)
	return query_string

def search_covers(search, api_key, secret_key):
    params = {
        'Operation': 'ItemSearch',
        'Keywords': str(search),
        'Version': '2009-01-06',
        'SearchIndex': 'Music',
        'Service': 'AWSECommerceService',
        'ResponseGroup': 'ItemAttributes,Images',
        }

    query_string = get_aws_query_string(str(api_key).strip(),
        str(secret_key).strip(), params)

    data = urllib.urlopen(query_string).read()

    # check for an error message
    m = re.search(r'<Message>(.*)</Message>', data, re.DOTALL)
    if m:
        logger.warning('Amazon Covers Search Error: %s' % m.group(1))
        raise AmazonSearchError(m.group(1))

    # check for large images
    regex = re.compile(r'<LargeImage><URL>([^<]*)', re.DOTALL)
    items = regex.findall(data)

    return items
